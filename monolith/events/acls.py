from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
    # URL = "https://api.pexels.com/v1/search"
    # headers = {"Authorization": PEXELS_API_KEY}
    # r = requests.get(URL, headers=headers)
    # pic_dict = json.loads(r.content)


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    URL1 = "http://api.openweathermap.org/geo/1.0/direct"
    r_location = requests.get(URL1, params=params)
    content = json.loads(r_location.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    URL2 = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(URL2, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None

    # URL1 = "http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit={limit}&appid={OPEN_WEATHER_API_KEY}"
    # header_loc = {"Authorization": OPEN_WEATHER_API_KEY}
    # r_location = requests.get(URL1, headers=header_loc)
    # loc_dict = json.loads(r_location.content)

    # URL2 = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # header_wea = {"Authorization": OPEN_WEATHER_API_KEY}
    # r_wea = requests.get(URL2, headers=header_wea)
    # wea_dict = json.loads(r_wea.content)
